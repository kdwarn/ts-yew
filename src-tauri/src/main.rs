#![cfg_attr(
    all(not(debug_assertions), target_os = "windows"),
    windows_subsystem = "windows"
)]

use chrono::prelude::*;
use timestudy::{self, Activity};

fn main() {
    tauri::Builder::default()
        .invoke_handler(tauri::generate_handler![current_activity, start, stop])
        .run(tauri::generate_context!())
        .expect("error while running tauri application");
}

#[tauri::command]
fn current_activity() -> Result<String, String> {
    match Activity::current_activity() {
        Ok(activity) => match activity {
            Some(_) => Ok("true".to_string()),
            None => Ok("false".to_string()),
        },
        Err(_) => Err("error".to_string()),
    }
}

/// Convert strings to DateTime
fn convert_dt(dt: &str) -> Result<DateTime<Utc>, String> {
    match dt.to_lowercase().as_str() {
        "today" => Ok(Utc::now()),
        _ => Err("error".to_string()),
    }
}

#[tauri::command]
// fn start(dt: String, tags: Option<Vec<String>>) -> Result<String, String> {
fn start() -> Result<String, String> {
    // convert datetime string to DateTime
    // let dt = convert_dt(&dt)?;

    match Activity::start(None, None) {
        Ok(_) => Ok("Started".to_string()),
        Err(_) => Err("error".to_string()),
    }
}

#[tauri::command]
fn stop() -> Result<String, String> {
    // let dt = if dt.is_some() {
    //     convert_dt(&dt.unwrap())?;
    // } else {
    //     None
    // };

    match Activity::stop(None) {
        Ok(_) => Ok("Activity stopped".to_string()),
        Err(_) => Err("error stopping activity".to_string()),
    }
}
