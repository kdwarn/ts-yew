use yew::prelude::*;

mod current_activity;
mod past_activities;
mod stats;

use current_activity::CurrentActivity;
use past_activities::PastActivities;
use stats::Stats;

#[function_component(App)]
fn app() -> Html {
    html! {
        <>
            <Stats />
            <CurrentActivity />
            <PastActivities />
        </>
    }
}

fn main() {
    yew::Renderer::<App>::new().render();
}
