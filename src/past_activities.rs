use yew::prelude::*;

pub struct PastActivities;

impl Component for PastActivities {
    type Message = ();
    type Properties = ();

    fn create(_ctx: &Context<Self>) -> Self {
        Self
    }

    fn view(&self, _ctx: &Context<Self>) -> Html {
        html! {
            <div class="past box">
                <h2>{ "Past Activities" }</h2>
            </div>
        }
    }
}
