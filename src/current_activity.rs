use wasm_bindgen::prelude::*;
// use web_sys::console::log_1;
use yew::{prelude::*, virtual_dom::AttrValue};

#[wasm_bindgen(module = "/public/glue.js")]
extern "C" {
    #[wasm_bindgen(js_name = invokeCurrentActivity, catch)]
    pub async fn get_current_activity() -> Result<JsValue, JsValue>;

    #[wasm_bindgen(js_name = invokeStart, catch)]
    pub async fn start() -> Result<JsValue, JsValue>;

    #[wasm_bindgen(js_name = invokeStop, catch)]
    pub async fn stop() -> Result<JsValue, JsValue>;
}

pub struct Button;

#[derive(Properties, PartialEq)]
pub struct ButtonProps {
    current_activity: bool,
}

impl Component for Button {
    type Message = ();
    type Properties = ButtonProps;

    fn create(_ctx: &Context<Self>) -> Self {
        Self
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        let class: &str;
        let text: &str;
        if ctx.props().current_activity {
            text = "Stop current activity";
            class = "button stop";
        } else {
            text = "Start new activity";
            class = "button start";
        };
        let parent = ctx.link().get_parent().unwrap().clone();

        html! {
            <button class={class} onclick={
                if ctx.props().current_activity {
                    parent.downcast::<CurrentActivity>().callback(|_| Msg::Stop)
                } else {
                    parent.downcast::<CurrentActivity>().callback(|_| Msg::Start)
                }
            }>{text}</button>
        }
    }
}

#[derive(Debug, Clone, PartialEq)]
pub enum Msg {
    GetStatus,
    SetStatus(bool),
    Start,
    Stop,
    MessageToUser(String),
}

pub struct CurrentActivity {
    status: bool,
    message: Option<AttrValue>,
}

impl Component for CurrentActivity {
    type Message = Msg;
    type Properties = ();

    fn create(_ctx: &Context<Self>) -> Self {
        Self {
            status: false,
            message: None,
        }
    }

    fn update(&mut self, ctx: &Context<Self>, msg: Self::Message) -> bool {
        let link = ctx.link();
        match msg {
            Msg::GetStatus => {
                // log_1(&JsValue::from("GetStatus"));
                link.send_future(async {
                    match get_current_activity().await {
                        Ok(v) => match v.as_string().unwrap().as_str() {
                            "true" => Msg::SetStatus(true),
                            "false" => Msg::SetStatus(false),
                            _ => Msg::SetStatus(false),
                        },
                        Err(e) => Msg::MessageToUser(e.as_string().unwrap()),
                    }
                });
            }
            Msg::SetStatus(status) => self.status = status,
            Msg::Start => {
                // log_1(&JsValue::from("Msg::Start"));
                link.send_future(async {
                    match start().await {
                        Ok(_) => Msg::GetStatus,
                        Err(e) => Msg::MessageToUser(e.as_string().unwrap()),
                    }
                })
            }
            Msg::Stop => {
                // log_1(&JsValue::from("This"));
                link.send_future(async {
                    match stop().await {
                        Ok(_) => Msg::GetStatus,
                        Err(e) => Msg::MessageToUser(e.as_string().unwrap()),
                    }
                })
            }
            Msg::MessageToUser(message) => {
                // log_1(&JsValue::from(message.clone()));
                self.message = Some(AttrValue::from(message));
            }
        };
        true
    }

    fn view(&self, _ctx: &Context<Self>) -> Html {
        let status_message = match self.status {
            true => "There is a current activity".to_string(),
            false => "No current activity".to_string(),
        };

        html! {
            <div class="current">
                <h2 class={"heading"}>{"Hello, WORLD!!"}</h2>
                <h3>{ status_message }</h3>
                <Button current_activity={self.status} />
                if let Some(v) = self.message.clone() {
                    <p>{v}</p>
                } else {
                    <p>{""}</p>
                }
            </div>
        }
    }

    fn rendered(&mut self, ctx: &Context<Self>, first_render: bool) {
        if first_render {
            ctx.link().send_message(Msg::GetStatus)
        }
    }
}
