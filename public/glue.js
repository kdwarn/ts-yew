const { invoke } = window.__TAURI__.tauri;

export function invokeCurrentActivity() {
  return invoke("current_activity"); // invoke returns a promise
}

export function invokeStart() {
  return invoke("start"); // invoke returns a promise
}

export function invokeStop() {
  return invoke("stop"); // invoke returns a promise
}
